const {STARTING_BALANCE} = require('../config');
const cryptoHash = require('../util/crypto-hash');
const {ec} = require('../util');
const Transaction = require('./transaction')

class Wallet{
    constructor(){
        this.balance = STARTING_BALANCE;
        this.keyPair = ec.genKeyPair();

        this.publicKey = this.keyPair.getPublic().encode('hex');
    }
    sign(data){
        return this.keyPair.sign(cryptoHash(data));
    }

    createTransactions({amount,recipient,chain}){
        if (chain) {
            this.balance = Wallet.calculateBalance({
                chain,
                address:this.publicKey
            });
        }
        if(amount>this.balance){
            throw new Error("Amount exceeds balance!");
        }
        return new Transaction({senderWallet:this,recipient,amount})
    }
    static calculateBalance({chain , address}){
        let hasConductedTrasnsaction = false;
        let outputsTotal =0;
        for(let i=chain.length-1;i>0;i--){
            const block = chain[i];
            for (let transaction of block.data) {
                if (transaction.input.address == address) {
                    hasConductedTrasnsaction =true;
                }

                const addressOutput = transaction.outputMap[address];
            
                if(addressOutput){
                    outputsTotal = outputsTotal + addressOutput;
                }
            }
            if(hasConductedTrasnsaction){
                break;
            }
        }
        return hasConductedTrasnsaction? outputsTotal:STARTING_BALANCE+outputsTotal;
    }

};
module.exports = Wallet;