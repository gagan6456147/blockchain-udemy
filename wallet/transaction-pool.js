const Transaction = require('./transaction');
class TransactionPool {
    constructor(){
        this.transactionMap = {};
    }

    clear(){
        this.transactionMap = {};
    }

    clearBlockChainTransaction({chain}){
        for(let i=1;i<chain.length;i++){
            const block = chain[i]
            for (let transaction of block.data){
                if(this.transactionMap[transaction.id]){
                    delete this.transactionMap[transaction.id];
                }
            }
        }

    }

    existingtransaction({inputAddress}){
        const transactions = Object.values(this.transactionMap);
        return transactions.find(transaction => transaction.input.address === inputAddress)
    }
    setMap(transactionMap){
        this.transactionMap = transactionMap;
    }
    setTransaction(transaction){
        this.transactionMap[transaction.id] = transaction;
    }
    validTransaction(){
        return Object.values(this.transactionMap).filter(
            transaction=> Transaction.validTransaction(transaction)
        );
    }
}

module.exports = TransactionPool;