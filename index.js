const express = require('express');
const bodyParser = require('body-parser');
const Blockchain = require('../Blockchain/blockhain/index');
const PubSub = require('./app/pubsub');
const TransactionPool = require('./wallet/transaction-pool');
const TransactionMiner = require('./app/transaction-miner');
const request = require('request');
const Wallet = require('./wallet')
const app = express();
const DEFAULT_PORT = 3000;
const blockchain = new Blockchain();

const transactionPool = new TransactionPool();
const ROOT_NODE_ADDRESS = `http://localhost:${DEFAULT_PORT}`;
const pubsub = new PubSub({blockchain,transactionPool});
const wallet = new Wallet();
const transactionMiner = new TransactionMiner({blockchain,transactionPool,wallet,pubsub});
let PEER_PORT;


app.use(bodyParser.json());

app.get('/api/blocks',(req,res)=>{
    res.json(blockchain.chain);
});

app.get('/api/mine-transactions',(req,res)=>{
    transactionMiner.mineTransaction();
    res.redirect('/api/blocks');
});

const syncWithRootNode = () =>{
   request({url: `${ROOT_NODE_ADDRESS}/api/blocks`},(error,response,body)=>{
    if (!error && response.statusCode === 200) {
        const rootChain = JSON.parse(body);
        console.log('response chain on a sync chain',rootChain);
        blockchain.replaceChain(rootChain);
    }
   });
   request({url: `${ROOT_NODE_ADDRESS}/api/transaction-pool-map`},(error,response,body)=>{
    if (!error && response.statusCode === 200) {
        const rootTransactionPoolMap = JSON.parse(body);
        console.log('replace transaction pool map on a sync chain',rootTransactionPoolMap);
        transactionPool.setMap(rootTransactionPoolMap);
    }
   });
};


app.post('/api/mine',(req,res)=>{
    const { data } = req.body;
    blockchain.addBlock({data});
    pubsub.broadcastChain();
    res.redirect('/api/blocks');
});


app.post('/api/transact',(req,res)=>{
    const {amount, recipient} = req.body;
    let transaction = transactionPool.existingtransaction({inputAddress:wallet.publicKey});
    try {
        if (transaction) {
            transaction.update({senderWallet:wallet,recipient,amount});
        } else {
            transaction = wallet.createTransactions({
                amount,
                recipient,
                chain:blockchain.chain
            });
        }
    } catch (error) {
       return res.status(400).json({
            type:'error',
            message:error.message
        })
    }
    
    
    transactionPool.setTransaction(transaction);
    pubsub.broadcastTransaction(transaction);
    res.json({
        type:'success',
        transaction
    });
});

app.get('/api/transaction-pool-map',(req,res)=>{
    res.json(transactionPool.transactionMap);
});

app.get('/api/wallet-info',(req,res)=>{
    const address  = wallet.publicKey;
    res.json({
        address,
        balance:Wallet.calculateBalance({chain:blockchain.chain,address})
    });
});


if (process.env.GENERATE_PEER_PORT === 'true'){
    PEER_PORT = DEFAULT_PORT + Math.ceil(Math.random()*1000)
}
const PORT = PEER_PORT || DEFAULT_PORT;
app.listen(PORT,()=>{
    console.log(`Localhost listening to host localhost:${PORT}`);
    syncWithRootNode();
});