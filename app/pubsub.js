const redis = require('redis');
const CHANNELS = {
    TEST:"TEST",
    BLOCKCHAIN:"BLOCKCHAIN",
    TRANSACTION:'TRANSACTION'
}

class PubSub{
    constructor({blockchain,transactionPool}){
        
        this.publisher = redis.createClient();
        this.subscriber = redis.createClient();
        this.blockchain = blockchain;
        this.transactionPool = transactionPool;
        this.subscribeToChannel();
        this.subscriber.on("message",(channel,message) => this.handleMessage(channel,message));
        
    }

    handleMessage(channel,message){
        console.log(`Message Recieved. Channel: ${channel}. Message: ${message}.`);
        
        const parsedMsg = JSON.parse(message);


        switch (channel) {
            case CHANNELS.BLOCKCHAIN:
                this.blockchain.replaceChain(parsedMsg,()=>{
                    this.transactionPool.clearBlockChainTransaction({
                        chain:parsedMsg
                    });
                });
                break;
            case CHANNELS.TRANSACTION:
                this.transactionPool.setTransaction(parsedMsg);
                break;
            default:
                return;
        }     
    }
    subscribeToChannel(){
        Object.values(CHANNELS).forEach(channel=>{
            //console.log(channel);
            this.subscriber.subscribe(channel);
        });
    
    }
    publish({ channel, message }) {
        this.subscriber.unsubscribe(channel, () => {
          this.publisher.publish(channel, message, () => {
            this.subscriber.subscribe(channel);
          });
        });
      }

    broadcastChain(){
        console.log('chain started!!')
        this.publish({
            channel:CHANNELS.BLOCKCHAIN,
            message:JSON.stringify(this.blockchain.chain)
        });
    }
    broadcastTransaction(transaction){
        this.publish({
            channel:CHANNELS.TRANSACTION,
            message:JSON.stringify(transaction)
        });
    }
}

module.exports = PubSub;

/*
const testPubsub = new PubSub({blockchain});

//setTimeout(() =>  testPubsub.publisher.publish(CHANNELS.TEST,'hello1') ,1000);
setTimeout(()=>testPubsub.broadcastChain(),2000);
*/