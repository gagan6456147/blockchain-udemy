const cryptoHash = require('./crypto-hash');

const hash = 'b2213295d564916f89a6a42455567c87c3f480fcd7a1c15e220f17d7169a790b'.toLowerCase();
describe('Crypto-Hash()',()=>{
    it('generates a Hash value',()=>{
        expect(cryptoHash('foo')).toEqual(hash);
    });
    it('produces the same hash for any order',()=>{
        expect(cryptoHash('one','two','three')).toEqual(cryptoHash('three','one','two'))
    });
});

