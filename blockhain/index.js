const Block = require('./block');
const cryptoHash = require('../util/crypto-hash');
const { REWARD_INPUT, MINING_REWARD } = require('../config');
const Transaction = require('../wallet/transaction');
const Wallet = require('../wallet');

class Blockchain{
    constructor(){
        this.chain = [Block.genesis()]
    }
    addBlock({data}){
        const newBlock = Block.minedBlock({
            lastBlock: this.chain[this.chain.length-1],
            data
        })
        this.chain.push(newBlock);
    }

    replaceChain(chain,validTransaction,onSuccess){
        if(chain.length <= this.chain.length){
            //console.error('The Incoming chain must be longer');
            return;
        }
        if (!Blockchain.isValidation(chain)) {
           // console.error('The Incoming chain must be valid');
            return;
        if (validTransaction && !this.validTransactionData({chain})){
            console.error('The incoming chain invalid data');
            return;
        }
        }
        if (onSuccess) onSuccess();
        this.chain= chain;
    }



    static isValidation(chain){

        if (JSON.stringify(chain[0]) !== JSON.stringify(Block.genesis())){
            return false;
        };
        for(let i=1;i<chain.length;i++){
            const {timestamp, data,nonce,difficulty, lastHash, hash} = chain[i];
            const actualLastHash = chain[i-1].hash;
            const predifficulty = chain[i-1].difficulty;
            if (actualLastHash !== lastHash){
                return false;
            };
            const validatedHash = cryptoHash(timestamp, data, nonce, difficulty ,lastHash);

            if (hash!== validatedHash){
                return false;
            };
            if (Math.abs(predifficulty-difficulty) >1){
                return false;
            }
            
        }
        return true
    };
    validTransactionData({ chain }) {
        for (let i=1; i<chain.length; i++) {
          const block = chain[i];
          const transactionSet = new Set();
          let rewardTransactionCount = 0;
    
          for (let transaction of block.data) {
            if (transaction.input.address === REWARD_INPUT.address) {
              rewardTransactionCount += 1;
    
              if (rewardTransactionCount > 1) {
                console.error('Miner rewards exceed limit');
                return false;
              }
    
              if (Object.values(transaction.outputMap)[0] !== MINING_REWARD) {
                console.error('Miner reward amount is invalid');
                return false;
              }
            } else {
              if (!Transaction.validTransaction(transaction)) {
                console.error('Invalid transaction');
                return false;
              }
    
              const trueBalance = Wallet.calculateBalance({
                chain: this.chain,
                address: transaction.input.address
              });
    
              if (transaction.input.amount !== trueBalance) {
                console.error('Invalid input amount');
                return false;
              }
    
              if (transactionSet.has(transaction)) {
                console.error('An identical transaction appears more than once in the block');
                return false;
              } else {
                transactionSet.add(transaction);
              }
            }
          }
        }
    
        return true;
      }
}

/*
const oneBlock = new Blockchain();
oneBlock.addBlock({data:'one'});
oneBlock.addBlock({data:'two'});
oneBlock.addBlock({data:'three'});
console.log(oneBlock);
*/



module.exports = Blockchain;