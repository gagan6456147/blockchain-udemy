const { describe, it, expect } = require("@jest/globals");
const Block = require('./block');
const { GENESIS_DATA,MINE_RATE }  = require('../config');
const cryptoHash = require("../util/crypto-hash");
const hexToBinary = require('hex-to-binary');

describe('Block()',()=>{
    const timestamp = 2000 ;
    const data = 'foo-data';
    const hash = 'foo-hash';
    const lastHash = 'foo-lastHash';
    const nonce = 1;
    const difficulty = 1;
    const block = new Block({timestamp,data,nonce,difficulty,hash,lastHash});
    
    it('has timestamp,data,hash and lastHash Property',()=>{
        expect(block.timestamp).toEqual(timestamp);
        expect(block.lastHash).toEqual(lastHash);
        expect(block.hash).toEqual(hash);
        expect(block.data).toEqual(data);
        expect(block.nonce).toEqual(nonce);
        expect(block.difficulty).toEqual(difficulty);
    
    });
    describe('genesis Function',() =>{
        const genesisBlock = Block.genesis();

        it('return a Block Instance',()=>{
            expect(genesisBlock instanceof Block).toBe(true)
        });
        it('return the gensis data',()=>{
            expect(genesisBlock).toEqual(GENESIS_DATA);
        });
    });
    describe('minedBlock()',()=>{
        const lastBlock = Block.genesis();
        const data = 'mined data';
        const minedBlock = Block.minedBlock({lastBlock,data});
    it('returns a mined instance',()=>{
        expect(minedBlock instanceof Block).toBe(true);
    });
    it('sets `lastHash` to be the `hash` of lastBLock',()=>{
        expect(minedBlock.lastHash).toEqual(lastBlock.hash);
    });
    it('sets the data',()=>{
        expect(minedBlock.data).toEqual(data);
    });
    it('sets the `timestamp`',()=>{
        expect(minedBlock.timestamp).not.toEqual(undefined);
    });
    it('creates the SHA-256 `Hash` based on proper input',()=>{
        expect(minedBlock.hash)
        .toEqual(
            cryptoHash(
                minedBlock.timestamp,
                lastBlock.hash,
                data,
                minedBlock.nonce,
                minedBlock.difficulty
                ));
    });
    it('sets a `hash` that matches the difficulty criteria',()=>{
        expect(hexToBinary(minedBlock.hash).substring(0,minedBlock.difficulty)).toEqual('0'.repeat(minedBlock.difficulty));
    });
    it('adjust the difficult',()=>{
        const possibleResult = [lastBlock.difficulty+1,lastBlock.difficulty-1]
        expect(possibleResult.includes(minedBlock.difficulty)).toBe(true);
    });
    });


    describe('adjustDifficulty',()=>{
        it('raises the difficulty for quickly mined block',()=>{
            expect(Block.adjustDifficulty({originalBlock:block,
                timestamp:block.timestamp+MINE_RATE-100}))
                .toEqual(block.difficulty+1);
        });
        it('drops the difficulty for slowly mined block',()=>{
            expect(Block.adjustDifficulty({originalBlock:block,
                timestamp:block.timestamp+MINE_RATE+100}))
                .toEqual(block.difficulty-1);
        });
        it('has a lower limit of 1',()=>{
            block.difficulty = -1;
            expect(Block.adjustDifficulty({originalBlock:block})).toEqual(1);
        });
    });
});

