const Blockchain = require('../blockhain/index');
const Block = require('./block');
const cryptoHash = require('../util/crypto-hash');
const Wallet = require('../wallet');
const Transaction = require('../wallet/transaction');

describe('Blockchain()',()=>{
  let blockchain,newChain,originalChain,errorMock;
  beforeEach(() => {
    blockchain = new Blockchain();
    newChain = new Blockchain();
    originalChain = blockchain.chain;
    errorMock = jest.fn();

    global.console.error = errorMock;
  
  });

  it('has chain array',()=>{
    expect(blockchain.chain instanceof Array).toBe(true);
  });
  it('has first element as genesis',()=>{
    expect(blockchain.chain[0]).toEqual(Block.genesis());
  });
  it('adds new block to the chain',()=>{
    const newData = 'foo-bar';
    blockchain.addBlock({data:newData});
    expect(blockchain.chain[blockchain.chain.length - 1].data).toEqual(newData);
  });

  describe('isValidation()',()=>{
    describe('when chain does not start with genesis block',()=>{
      it('returns false',()=>{
        blockchain.chain[0] = {data:"fake-genesis"}
        expect(Blockchain.isValidation(blockchain.chain)).toBe(false);
      });
    });
    describe('when chain starts with genesis block and multiple blocks',()=>{
      beforeEach(()=>{
        blockchain.addBlock({data:"one"});
        blockchain.addBlock({data:"two"});
        blockchain.addBlock({data:"three"});
      });
      describe('and a last referenced changed',()=>{
        it('return false',()=>{

          blockchain.chain[2].lastHash = 'broken-Hash';
          expect(Blockchain.isValidation(blockchain.chain)).toBe(false);
        });
      });

      describe('and a chain contains block with an invalid field',()=>{
        it('return false',()=>{
          blockchain.chain[2].data = 'some-evil-code';
          //console.log(blockchain);
          expect(Blockchain.isValidation(blockchain.chain)).toBe(false);
          
        });
      });
      describe('and the chain contains a block with jumped difficulty',()=>{
        it('returns false',()=>{
          const lastBLock = blockchain.chain[blockchain.chain.length-1];
          const lastHash = lastBLock.hash;
          const timestamp = Date.now();
          const nonce = 0;
          const data = [];
          const difficulty = lastBLock.difficulty -3;
          const hash = cryptoHash(timestamp,lastHash,nonce,data,difficulty);
          const badBlock = new Block({timestamp,lastHash,nonce,data,difficulty,hash});
          blockchain.chain.push(badBlock);

          expect(Blockchain.isValidation(blockchain.chain)).toBe(false);
        });
      });
      describe('does not contain any invalid blocks',()=>{
        it('returns true',()=>{
          expect(Blockchain.isValidation(blockchain.chain)).toBe(true);
          
        });
      });
    });
  });

  describe('replaceChain()',()=>{
    describe('when the new chain is not longer',()=>{
      it('does not replace the chain',()=>{
        newChain.chain[0] = {new:'chain'};
        blockchain.replaceChain(newChain.chain);
        expect(blockchain.chain).toEqual(originalChain);
      });
    });
    describe('when the new chain is longer',()=>{
      beforeEach(()=>{
        newChain.addBlock({data:"one"});
        newChain.addBlock({data:"two"});
        newChain.addBlock({data:"three"});
      });
      describe('and the chain is invalid',()=>{
        it('does not replace the chain',()=>{
          newChain.chain[2].hash = 'some-fake-hash';
          expect(blockchain.chain).toEqual(originalChain);
        });
      });
      describe('and the chain is valid',()=>{
        it('replaces the chain',()=>{
          blockchain.replaceChain(newChain.chain);
          expect(blockchain.chain).toEqual(newChain.chain);
  
        });
      });
    });
  });
  describe('validTransactionData()', () => {
    let transaction, rewardTransaction, wallet;

    beforeEach(() => {
      wallet = new Wallet();
      transaction = wallet.createTransactions({ recipient: 'foo-address', amount: 65 });
      rewardTransaction = Transaction.rewardTransaction({ minerWallet: wallet });
    });

    describe('and the transaction data is valid', () => {
      it('returns true', () => {
        newChain.addBlock({ data: [transaction, rewardTransaction] });

        expect(blockchain.validTransactionData({ chain: newChain.chain })).toBe(true);
        expect(errorMock).not.toHaveBeenCalled();
      });
    });

    describe('and the transaction data has multiple rewards', () => {
      it('returns false and logs an error', () => {
        newChain.addBlock({ data: [transaction, rewardTransaction, rewardTransaction] });

        expect(blockchain.validTransactionData({ chain: newChain.chain })).toBe(false);
        expect(errorMock).toHaveBeenCalled();
      });
    });

    describe('and the transaction data has at least one malformed outputMap', () => {
      describe('and the transaction is not a reward transaction', () => {
        it('returns false and logs an error', () => {
          transaction.outputMap[wallet.publicKey] = 999999;

          newChain.addBlock({ data: [transaction, rewardTransaction] });

          expect(blockchain.validTransactionData({ chain: newChain.chain })).toBe(false);
          expect(errorMock).toHaveBeenCalled();
        });
      });

      describe('and the transaction is a reward transaction', () => {
        it('returns false and logs an error', () => {
          rewardTransaction.outputMap[wallet.publicKey] = 999999;

          newChain.addBlock({ data: [transaction, rewardTransaction] });

          expect(blockchain.validTransactionData({ chain: newChain.chain })).toBe(false);
          expect(errorMock).toHaveBeenCalled();
        });
      });
    });
    
    describe('and the transaction data has at least one malformed input', () => {
      it('returns false and logs an error', () => {
        wallet.balance = 9000;

        const evilOutputMap = {
          [wallet.publicKey]: 8900,
          fooRecipient: 100
        };

        const evilTransaction = {
          input: {
            timestamp: Date.now(),
            amount: wallet.balance,
            address: wallet.publicKey,
            signature: wallet.sign(evilOutputMap)
          },
          outputMap: evilOutputMap
        }

        newChain.addBlock({ data: [evilTransaction, rewardTransaction] });

        expect(blockchain.validTransactionData({ chain: newChain.chain })).toBe(false);
        expect(errorMock).toHaveBeenCalled();
      });
    });
    
    describe('and a block contains multiple identical transactions', () => {
      it('returns false and logs an error', () => {
        newChain.addBlock({
          data: [transaction, transaction, transaction, rewardTransaction]
        });

        expect(blockchain.validTransactionData({ chain: newChain.chain })).toBe(false);
        expect(errorMock).toHaveBeenCalled();
      });
    });
  });
});