const hexToBinary = require('hex-to-binary');
const { GENESIS_DATA,MINE_RATE } = require('../config');
const cryptoHash = require('../util/crypto-hash');

class Block{
    constructor({timestamp,data,hash,lastHash,nonce,difficulty}){
        this.data = data;
        this.hash = hash;
        this.lastHash = lastHash;
        this.timestamp = timestamp;
        this.difficulty = difficulty;
        this.nonce = nonce;
    }
    static genesis(){
        return new this(GENESIS_DATA);
    }
    static minedBlock({lastBlock,data}){
        const lastHash = lastBlock.hash;
        let timestamp,hash;
        let { difficulty } = lastBlock;
        let nonce = 0;
        
        do {
            nonce++;
            timestamp= Date.now();
            difficulty = Block.adjustDifficulty({originalBlock:lastBlock,timestamp})
            hash = cryptoHash(timestamp,difficulty,nonce,lastHash,data);

        } while (hexToBinary(hash).substring(0,difficulty) !== '0'.repeat(difficulty));
        return new this({
            timestamp,
            lastHash,
            data,
            difficulty,
            nonce,
            hash
        });
    }
    static adjustDifficulty({originalBlock,timestamp}){
        const {difficulty} = originalBlock;

        if (difficulty <1) return 1;
        
        if((timestamp-originalBlock.timestamp) > MINE_RATE) {
            return difficulty-1;
        }
        return difficulty+1;
    }
}

/*
const fooBlock = new Block({
    timestamp:'01/01/2020',
    data:'foo-data',
    hash:'foo-hash',
    lastHash: 'foo-lastHash'
});
console.log(fooBlock);
*/


module.exports = Block;
